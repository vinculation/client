import React from 'react';
import { shallow } from 'enzyme';
import { Provider } from 'react-redux';

import ProtectedRoute from '../index';
import store from '../../../store';

describe('<ProtectedRoute />', () => {
  it('Should renders without crashing', () => {
    const component = shallow(
      <Provider store={store}>
        <ProtectedRoute isLogIn />
      </Provider>,
    );
    expect(component).toMatchSnapshot();
  });
});
