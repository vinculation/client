import React, { Suspense, lazy, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Route, Switch, Redirect } from 'react-router-dom';
import { checkingSession } from 'slices/user.slice';
import LoadingComponent from 'components/Loading';

const ProtectedRoute = lazy(() => import('components/ProtectedRoute'));
const NotFoundPage = lazy(() => import('components/Pages/NotFound'));
const SignInPage = lazy(() => import('containers/Login'));

export const Routes = () => {
	const dispatch = useDispatch();
	const { checking, session } = useSelector(state => state.users);
	let permissions = [];

	useEffect(() => {
		dispatch(checkingSession());
	}, [dispatch]);

	if (checking) {
		return <LoadingComponent />;
	}

	if (session) permissions = session.permissions;

	return (
		<Suspense
			fallback={<LoadingComponent />}
		>
			<Switch>
				<ProtectedRoute
					isLogIn
					path="/sign-in"
					component={SignInPage}
				/>
				{
					!session && permissions.length === 0 && <Redirect to="/sign-in" />
				}
				{
					permissions && Array.isArray(permissions) && permissions
						.map((rute) => {
							let myComponent = null;
							
							try {
								myComponent = require(`containers/${rute.module.component}`); // eslint-disable-line import/no-dynamic-require,global-require,max-len
							} catch (error) {
								return null;
							}

							if (!myComponent) return null;

							return (
								<ProtectedRoute
									exact={rute.module.exact}
									key={rute.module._id}
									path={(rute && rute.module && rute.module.route) || ''}
									component={myComponent && myComponent.default}
								/>
							);
						})
						.filter(r => !!r)}

				<Route
					path="*"
				>
					<NotFoundPage />
				</Route>
			</Switch>
		</Suspense>
	);
};

export const FILE_STATUS_PAGE = '/';
