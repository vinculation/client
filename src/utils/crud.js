import React from 'react';
import { CheckBox, CancelSharp } from '@material-ui/icons';
import { humanize, dateFormat } from './format';

const columnsDefaultProps = {
  flex: 1,
};

export const getEndpoint = (endpoint, params = null) => {
  const string = String.prototype;
  const query = [];

  if (params) {
    Object.entries(params).forEach((values) => {
      query.push(`${values[0]}=${values[1]}`);
    });
  }
  return string.concat(endpoint, '?', query.join('&'));
};

export function getProperty(value) {
  let property;

  if (!value) {
    return null;
  }

  if (Object.prototype.hasOwnProperty.call(value, 'description')) {
    property = 'description';
  }
  if (Object.prototype.hasOwnProperty.call(value, 'firstName')) {
    property = 'firstName';
  }
  if (Object.prototype.hasOwnProperty.call(value, 'name')) {
    property = 'name';
  }

  return property;
}

export const getRowData = (data = []) => data.map((row) => ({ ...row, id: row._id }));

export const getColumns = (data, children, alias = {}) => {
  const hide = true;
  if (!data || (Array.isArray(data) && data.length === 0)) return [];

  const columns = Object
    .entries(data[0])
    .map(([key, value]) => {
      let aliasKey;
      if (Object.keys(alias)) {
        if (alias[key] && typeof value === 'object') aliasKey = alias[key];
      }
      if (typeof value === 'object') {
        if (value) {
          return ({
            field: key,
            headerName: humanize(key),
            renderCell: ({ row, value: val }) => (row && val) && (
              aliasKey ? row[aliasKey] : val[getProperty(value)]
            ),
            ...columnsDefaultProps,
          });
        }
        return ({ field: key, headerName: humanize(key), hide });
      }
      if (key.includes('Date')) {
        return (
          {
            field: key,
            headerName: humanize(key),
            type: 'dateTime',
            valueFormatter: (date) => dateFormat(date.value),
            ...columnsDefaultProps,
          }
        );
      }
      if (typeof value === 'boolean') {
        return ({
          field: key,
          headerName: humanize(key),
          renderCell: (item) => {
            const { value: val } = item;

            return (
              val
                ? <CheckBox style={{ color: 'green' }} />
                : <CancelSharp style={{ color: 'red' }} />
            );
          },
          hide,
          ...columnsDefaultProps,
        });
      }
      return (
        {
          field: key,
          headerName: humanize(key),
          hide: key.includes('id'),
          ...columnsDefaultProps,
        }
      );
    });
  columns.push({
    field: 'actions',
    headerName: 'Actions',
    width: 200,
    renderCell: children,
  });

  return columns;
};

export function getSort(sort) {
  if (sort.length > 0) {
    return sort[0].sort === 'desc'
      ? String.prototype.concat('-', sort[0].field)
      : sort[0].field;
  }
  return null;
}
