import _ from 'lodash';
import dayjs from 'dayjs';
import localizedFormat from 'dayjs/plugin/localizedFormat';
import * as models from '../models';

dayjs.extend(localizedFormat);

export const getModel = (modelName) => {
  if (models[modelName]) {
    return models[modelName];
  }
  return null;
};

export const getAttributes = (modelName) => {
  const model = getModel(modelName);

  if (model && model.hasOwnProperty('attributes')) {
    return model.attributes;
  }

  return null;
};

export const getModelName = (relModelName, attribute) => {
  const relModel = getAttributes(relModelName);

  if (relModel && relModel.hasOwnProperty(attribute)) {
    return relModel[attribute].model || relModel[attribute].collection;
  }

  return null;
};

export const hasModel = (relModelName, attribute) => !!getModelName(relModelName, attribute);

export const getAttribute = (modelName, attribute) => {
  const attributes = getAttributes(modelName);
  if (attributes && attributes.hasOwnProperty(attribute)) {
    return attributes[attribute];
  }

  return null;
};

export const getAttributeType = (modelName, attr) => {
  const attribute = getAttribute(modelName, attr);

  if (attribute) {
    switch (true) {
      case attribute.hasOwnProperty('model'):
        return 'model';

      case attribute.hasOwnProperty('collection'):
        return 'collection';

      case attribute.hasOwnProperty('api'):
        return 'api';

      case attribute.hasOwnProperty('columnType'):
        return attribute.columnType;

      default:
        return attribute.type;
    }
  } else if (attr === 'createdAt' || attr === 'updatedAt' || attr === 'deletedAt') {
    return 'datetime';
  }

  return null;
};

export const valueTypeAdapter = (values) => _.mapValues(values, (i) => {
  const date = dayjs(i);
  const { length } = `${i}`;

  if (date.isValid() && length === 13) {
    return date.format('YYYY-MM-DD HH:mm:ss');
  }

  return i;
});

export const getPopulate = (modelName) => {
  const model = getModel(modelName);

  if (model && model.hasOwnProperty('populate')) {
    return model.populate;
  }

  return null;
};
