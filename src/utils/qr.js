import React from 'react';
import {
  Grid,
  Paper,
} from '@material-ui/core';
import { CloudDownload } from '@material-ui/icons';
import { humanize } from 'utils/format';
import API from 'services/api';

const handleDownload = (name, items) => {
  API.get(`/${name}/download/qr`, {
    responseType: 'blob',
    headers: {
      'Content-Type': 'application/json',
    },
    params: { items },
  })
    .then((resp) => resp.data)
    .then(async (blob) => {
      const file = new Blob([blob], { type: 'application/pdf' });
      const fileURL = URL.createObjectURL(file);
      window.open(fileURL);
    });
};

export const tools = (name, items) => (
  <Grid>
    <Paper className="crud-page-statistics click" level={2} onClick={() => handleDownload(name, items)}>
      <div className="crud-page-statistics-title">
        <CloudDownload />
      </div>
      <div className="crud-page-statistics-text">
        {humanize('Download')}
      </div>
    </Paper>
  </Grid>
);
