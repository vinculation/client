import React from 'react';
import {
  getEndpoint,
  getProperty,
  getRowData,
  getColumns,
  getSort,
} from '../crud';

describe('getEndpoint', () => {
  it('should not throw if passed valid store shape', () => {
    expect(() => getEndpoint('received-orders', { populate: ['company', 'user'] })).not.toThrow();
    expect(() => getEndpoint('received-orders')).not.toThrow();
  });
});

describe('getProperty', () => {
  it('should not throw if passed valid store shape', () => {
    expect(() => getProperty({ description: true })).not.toThrow();
    expect(() => getProperty({ firstName: true })).not.toThrow();
    expect(() => getProperty({ name: true })).not.toThrow();
    expect(() => getProperty(null)).not.toThrow();
  });
});

describe('getRowData', () => {
  it('should not throw if passed valid store shape', () => {
    expect(() => getRowData(
      [
        {
          description: true,
          _id: 1,
        },
        {
          description: true,
          _id: 2,
        },
      ],
    )).not.toThrow();
  });
});

describe('getColumns', () => {
  it('should not throw if passed valid store shape', () => {
    expect(() => getColumns(
      [
        {
          description: true,
          _id: 1,
          shipAddress: { name: 'pepe' },
          invoiceDate: '2020-01-01',
          deletedAt: null,
        },
        {
          description: true,
          _id: 2,
        },
      ],
    )).not.toThrow();
    expect(() => getColumns(
      [
        { description: true, _id: 1, shipAddress: { name: 'pepe' } },
        { description: true, _id: 2 },
      ],
      <h2>Test</h2>,
      {
        shipAddress: 'name',
      },
    )).not.toThrow();
    expect(() => getColumns(
      [
        { description: true, _id: 1, shipAddress: { name: 'pepe' } },
        { description: true, _id: 2 },
      ],
      <h2>Test</h2>,
    )).not.toThrow();
  });
});

describe('getSort', () => {
  it('should not throw if passed valid store shape', () => {
    expect(() => getSort([{ field: 'name', sort: 'desc' }])).not.toThrow();
    expect(() => getSort([{ field: 'name', sort: 'asc' }])).not.toThrow();
    expect(() => getSort([])).not.toThrow();
  });
});
