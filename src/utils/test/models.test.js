import {
  getModel,
  getAttributes,
  getModelName,
  hasModel,
  getAttribute,
  getAttributeType,
  valueTypeAdapter,
  getPopulate,
  // getBelongsToValue,
  // getAttributeAssociation,
} from '../models';

describe('getModel', () => {
  it('should not throw if passed valid store shape', () => {
    expect(() => getModel('receiveOrders')).not.toThrow();
    expect(() => getModel('receive')).not.toThrow();
  });
});

describe('getAttributes', () => {
  it('should not throw if passed valid store shape', () => {
    expect(() => getAttributes('receiveOrders')).not.toThrow();
    expect(() => getAttributes('receive')).not.toThrow();
  });
});

describe('getModelName', () => {
  it('should not throw if passed valid store shape', () => {
    expect(() => getModelName('receiveOrders', 'number')).not.toThrow();
    expect(() => getModelName('receive', 'number')).not.toThrow();
  });
});

describe('hasModel', () => {
  it('should not throw if passed valid store shape', () => {
    expect(() => hasModel('receiveOrders', 'number')).not.toThrow();
    expect(() => hasModel('receive', 'number')).not.toThrow();
  });
});

describe('getAttribute', () => {
  it('should not throw if passed valid store shape', () => {
    expect(() => getAttribute('receiveOrders', 'number')).not.toThrow();
    expect(() => getAttribute('receive', 'number')).not.toThrow();
  });
});

describe('getAttributeType', () => {
  it('should not throw if passed valid store shape', () => {
    expect(() => getAttributeType('receiveOrders', 'company')).not.toThrow();
    expect(() => getAttributeType('receiveOrders', 'number')).not.toThrow();
    expect(() => getAttributeType('receive', 'createdAt')).not.toThrow();
    expect(() => getAttributeType('receive', 'name')).not.toThrow();
  });
});

describe('valueTypeAdapter', () => {
  it('should not throw if passed valid store shape', () => {
    const values = [{
      model: 'shipping',
      deliveredAt: '2020-01-01T12:00:00',
    }];
    expect(() => valueTypeAdapter(values.deliveredAt)).not.toThrow();
    expect(() => valueTypeAdapter(values.deliveredAt)).not.toThrow();
  });
});

describe('getPopulate', () => {
  it('should not throw if passed valid store shape', () => {
    expect(() => getPopulate('receiveOrders')).not.toThrow();
    expect(() => getPopulate('receive')).not.toThrow();
  });
});
