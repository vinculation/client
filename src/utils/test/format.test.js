import {
  wordToColor,
  formatNumber,
  humanize,
  transformDate,
  hashCode,
  dateFormat,
  parseAddress,
  getValue,
} from '../format';

describe('hashCode', () => {
  it('should not throw if passed valid store shape', () => {
    expect(() => hashCode()).not.toThrow();
    expect(() => hashCode('test')).not.toThrow();
  });
});

describe('wordToColor', () => {
  it('should not throw if passed valid store shape', () => {
    expect(() => wordToColor()).not.toThrow();
    expect(() => wordToColor('test')).not.toThrow();
  });
});

describe('formatNumber', () => {
  it('should not throw if passed valid store shape', () => {
    expect(() => formatNumber(null)).not.toThrow();
    expect(() => formatNumber(25)).not.toThrow();
    expect(() => formatNumber(25.3333, { decimals: 2 })).not.toThrow();
  });
});

describe('humanize', () => {
  it('should not throw if passed valid store shape', () => {
    expect(() => humanize('_id')).not.toThrow();
    expect(() => humanize('my_test')).not.toThrow();
    expect(() => humanize('myTest')).not.toThrow();
  });

  it('should throw if passed invalid store shape', () => {
    expect(() => humanize()).toThrow();
    expect(() => humanize(1)).toThrow();
    expect(() => humanize({})).toThrow();
  });
});

describe('transformDate', () => {
  it('should not throw if passed valid store shape', () => {
    expect(() => transformDate(null, '2021-03-13T23:53:16.880Z')).not.toThrow();
    expect(() => transformDate('2021-03-12T23:53:16.880Z', '2021-03-13T23:53:16.880Z')).not.toThrow();
    expect(() => transformDate('2021-03-12T23:53:16.880Z', '2021-03-25T23:53:16.880Z')).not.toThrow();
  });
});

describe('dateFormat', () => {
  it('should not throw if passed valid store shape', () => {
    expect(() => dateFormat(null)).not.toThrow();
    expect(() => dateFormat('2021-03-12T23:53:16.880Z')).not.toThrow();
    expect(() => dateFormat('2021-03-12T23:53:16.880Z', true)).not.toThrow();
  });
});

describe('parseAddress', () => {
  it('should not throw if passed valid store shape', () => {
    expect(() => parseAddress(null)).not.toThrow();
    expect(() => parseAddress({
      address1: 'Test1',
      address2: 'Test2',
      city: 'Tijuana',
      companyName: 'G-Global',
      country: 'Mexico',
      name: 'Antonio Orozco',
      postalCode: '22222',
      state: 'Baja California',
    })).not.toThrow();
    expect(() => parseAddress({
      address1: 'Test1',
      address2: 'Test2',
      city: 'Tijuana',
      companyName: 'G-Global',
      country: 'Mexico',
      name: 'Antonio Orozco',
      postalCode: '22222',
      state: 'Baja California',
    }, '-')).not.toThrow();
  });
});

describe('getValue', () => {
  it('should not throw if passed valid store shape', () => {
    expect(() => getValue({
      t: 'Test1',
      sub: { val: 'test' },
    }, 'sub.val')).not.toThrow();
  });
});
