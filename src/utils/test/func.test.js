import { isObjectId } from '../func';

describe('isObjectId', () => {
  it('should not throw if passed valid store shape', () => {
    expect(() => isObjectId('test')).not.toThrow();
  });
});
