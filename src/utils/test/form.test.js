import { FormEnumTypes } from 'constants/enums';
import { getValues, rowDefinition } from '../forms';

const data = {
  action: 'edit',
  fields: [
    {
      key: 'field1',
      value: 'field 1',
      type: FormEnumTypes.textfield,
      validation: {
        errorMessage: 'field1 required',
        regex: /^\s*$/,
      },
    },

    {
      key: 'field2',
      value: 'ABC',
      type: FormEnumTypes.textfield,
      validation: {
        errorMessage: 'field2 required',
        regex: /^\s*$/,
      },
    },

    {
      key: 'field3',
      value: 123,
      type: FormEnumTypes.textfield,
    },
  ],
};

describe('getValues', () => {
  it('should extract the data in object more simple', () => {
    const expected = [
      {
        field1: 'field 1',
      },
      {
        field2: 'ABC',
      },
      {
        field3: '123',
      },
    ];
    expect(getValues(data.fields)).toEqual(expected);
  });
});

describe('rowDefinition', () => {
  it('should extract the data in object more simple', () => {
    const object = {
      _id: '6051099ae88a881a0597a7f3',
      number: 'EJAMT0001',
      type: {
        _id: '603989660f7f08025b84168c',
        name: 'Receive',
        table: 'orders',
        active: true,
      },
      status: {
        _id: '603989c50f7f08025b84168e',
        name: 'Ready to Pick',
        color: 'blue',
        table: 'orders',
        type: '603989ec0f7f08025b84168f',
        parentStatus: '603989c50f7f08025b84168e',
        active: true,
      },
      shipAddress: {
        firstName: 'Antonio Orozco',
        lastName: 'Orozco',
        companyName: 'G-Global',
        address1: 'Test1',
        address2: 'Test2',
        city: 'Tijuana',
        state: 'Baja California',
        country: 'Mexico',
        postalCode: '22222',
      },
      billAddress: {
        firstName: 'Antonio Orozco',
        lastName: 'Orozco',
        companyName: 'G-Global',
        address1: 'Test1',
        address2: 'Test2',
        city: 'Tijuana',
        state: 'Baja California',
        country: 'Mexico',
        postalCode: '22222',
      },
      notes: 'es un test',
      receivedDate: '2020-01-01T08:00:00.000Z',
      deliveryDate: '2020-01-01T08:00:00.000Z',
      deletedAt: null,
      company: {
        _id: '60346bac72208214676e15ce',
        name: 'G-Global Logistic',
        socialName: 'G-Global Logistic Inc.',
        active: true,
      },
      active: true,
    };

    expect(() => rowDefinition(object, 'edit')).not.toThrow();
  });
});
