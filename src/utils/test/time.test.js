import { getDateFormated } from '../time';

describe('getDateFormated', () => {
  it('should not throw if passed valid store shape', () => {
    expect(() => getDateFormated()).not.toThrow();
    expect(() => getDateFormated({})).not.toThrow();
  });
});
