import { humanize } from './format';
import { getProperty } from './crud';
import { FormEnumTypes } from '../constants/enums';

export const getValues = (fields) => {
  const onlyValues = [];
  fields.forEach((item) => {
    onlyValues.push(JSON.parse(`{"${item.key}": "${item.value ? item.value : ''}"}`));
  });
  return onlyValues;
};

export const rowDefinition = (data, action) => ({
  action,
  fields: Object.entries(data).map((item) => {
    const key = item[0];
    let value = item[1];

    if (typeof value === 'object') {
      if (value) {
        value = value[getProperty(value)];
      }
    }

    return {
      key: humanize(key),
      value,
      disabled: false,
      type: FormEnumTypes[typeof value],
      validation: {
        errorMessage: 'error',
        regex: 'regex',
      },
    };
  }),
});
