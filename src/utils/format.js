import dayjs from 'dayjs';
import localizedFormat from 'dayjs/plugin/localizedFormat';

dayjs.extend(localizedFormat);

const dateFormatString = 'DD/MM/YYYY HH:mm:ss';

export const hashCode = (str = 'open') => {
  let hash = 0;
  for (let i = 0; i < str.length; i += 1) {
    hash = str.charCodeAt(i) + ((hash << 5) - hash); //eslint-disable-line
  }
  return hash;
};

const intToRGB = (i) => {
  const c = (i & 0x00ffffff).toString(16).toUpperCase(); //eslint-disable-line

  return '00000'.substring(0, 6 - c.length) + c;
};

export const wordToColor = (str) => `#${intToRGB(hashCode(str))}`;

export const formatNumber = (number, options = { decimals: false, prefix: null }) => {
  if (!number) return 'NULL';

  if (options.decimals) {
    const value = Number(number).toFixed(options.decimals).replace(/\d(?=(\d{3})+\.)/g, '$&,');

    return `${options.prefix} ${value}`.trim();
  }
  return number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
};

export const humanize = (str) => {
  if (str.includes('_')) {
    let i;
    const frags = str.split('_');
    for (i = 0; i < frags.length; i += 1) {
      frags[i] = frags[i].charAt(0).toUpperCase() + frags[i].slice(1);
    }
    const word = frags.join(' ');

    if (String(word).length <= 3) return word.toUpperCase();

    return word;
  }
  return str
    .replace(/([A-Z])/g, ' $1')
    .replace(/^./, (string) => string.toUpperCase());
};

export const transformDate = (startDate, endDate) => {
  if (!startDate || !endDate) return 'N/A';

  const date1 = dayjs(startDate);
  const date2 = dayjs(endDate);
  const days = date2.diff(date1, 'days');

  if (days > 1) {
    const hours = date2.diff(date1, 'hours') - days * 24;
    return `${days} days , ${hours} hours`;
  }

  return `${date2.diff(date1, 'hours')} hours`;
};

export const isHexadecimal = (string) => {
  const re = /[0-9A-Fa-f]{6}/g;
  let isValid = false;

  if (re.test(string)) {
    isValid = true;
  }

  re.lastIndex = 0;

  return isValid;
};

export const camelize = (str) => str
  .replace(/(?:^\w|[A-Z]|\b\w)/g, (word, index) => (
    index === 0 ? word.toLowerCase() : word.toUpperCase()
  )).replace(/\s+/g, '');

export const dateFormat = (date, humanizeDate = false) => {
  if (!date) return 'NULL';
  return dayjs(date).format(humanizeDate ? 'lll' : dateFormatString);
};

export const parseAddress = (address, separator = ' ') => {
  if (!address) return 'NULL';

  let str = '-';
  const addressOrder = [
    'name',
    'address1',
    'address2',
    'city',
    'state',
    'country',
    'postalCode',
    'phone',
  ];

  addressOrder.forEach((property) => {
    if (address[property]) {
      str += separator + address[property];
    }
  });

  return str;
};

export const getValue = (values, keyString) => {
  const keys = keyString.split('.');

  let value = values;
  keys.forEach((k) => {
    if (value[k]) value = value[k];
  });

  return typeof (value) === 'string' ? value : 'NULL';
};
