import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

import './App.scss';

import { Routes } from '../constants/router';

const App = () => (
	<Router>
		<Routes />
	</Router>
);

export default App;
